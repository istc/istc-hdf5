#!/bin/bash

# start the simulation and let it run for 10 steps
./simulation.py --run 10 --restart_file_name stochastic.h5

# examine the restart file with h5dump

h5dump stochastic.h5 | less

# restart it from step 5, let it run 20 more steps
./simulation.py --restart 5 --run 20 --restart_file_name stochastic.h5


